package udem.edu.co.entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-04-06T02:10:30")
@StaticMetamodel(Vehiculos.class)
public class Vehiculos_ { 

    public static volatile SingularAttribute<Vehiculos, String> tipo;
    public static volatile SingularAttribute<Vehiculos, String> estado;
    public static volatile SingularAttribute<Vehiculos, String> estacion;
    public static volatile SingularAttribute<Vehiculos, Integer> id;
    public static volatile SingularAttribute<Vehiculos, String> placa;

}